var app = angular.module( 'wordGame', [] );

app.controller( 'wordController', [ '$scope', '$timeout', function( $scope, $timeout ){
		// $scope.demo = "failed states";
	var words = ["library","market","rodents","kampala","vinegar"];
		//$scope.demo = "stringify";
		$scope.wrongChoices = [];
		$scope.RightChoices = [];
		$scope.guessNumber = 5;
		$scope.wordInView = '';
		$scope.input = {
			character : ''
		}

	var chooseRandomWord = function(){
		var index = Math.round(Math.random()*(words.length - 1));
		return words[ index ];
		// console.log( words[ index ] );
	}

	var newGame = function(){
		$scope.wrongChoices = [];
		$scope.RightChoices = [];
		$scope.guessNumber = 5;
		$scope.wordInView = '';
		chosenWord = chooseRandomWord();
		var staredWord = '';

		for( var i=0; i<chosenWord.length; i++ ){
			staredWord += '*';
		}

		// console.log( staredWord );
		$scope.wordInView = staredWord;
	}

	$scope.characterCheck = function(){
		
		for( var i =0; i < $scope.RightChoices.length; i++ ){
			if( $scope.RightChoices[i].toLowerCase() == $scope.input.character.toLowerCase ){
				$scope.input.character = "";
				return;
			}
		}

		for( var i =0; i < $scope.wrongChoices.length; i++ ){
			if( $scope.wrongChoices[i].toLowerCase() == $scope.input.character.toLowerCase ){
				$scope.input.character = "";
				return;
			}
		}

		var correct = false;
		for(var i=0; i<chosenWord.length; i++){
			if( chosenWord[i].toLowerCase() == $scope.input.character.toLowerCase() ){
				$scope.wordInView = $scope.wordInView.slice(0,i)+$scope.input.character.toLowerCase()+$scope.wordInView.slice(i+1);
				correct =true;
			}
		}

		if( correct ){
			$scope.RightChoices.push( $scope.input.character.toLowerCase());
		}else{
			$scope.guessNumber --;
			$scope.wrongChoices.push( $scope.input.character.toLowerCase());
		}

		$scope.input.character = "";
		if( $scope.guessNumber == 0 ){
			 // alert('Game Over!');

			$timeout(function(){
				newGame();
			}, 5000);
			
		}

		if( $scope.wordInView.indexOf( "*" ) == -1 ){
			 // alert('Kudos You Won!');
			$timeout(function(){
				newGame();
			}, 5000);
		}


	}

	newGame();



	

}] );